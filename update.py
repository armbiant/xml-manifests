#!/usr/bin/env python3

import os
import re
import shutil
import subprocess
import time
import pathlib
import logging
import json

import requests

logging.basicConfig(level=logging.INFO)

sess = requests.session()
g_cachebust_ts = int(time.time())

def download(url, output_path, cachebust=False):
    log = logging.getLogger("download")
    params = {}
    if cachebust:
        params = {"cachebust": g_cachebust_ts}
    log.info(f"Requesting {url}")
    resp = sess.get(url, params=params, stream=True)
    if resp.status_code == 200:
        with open(output_path, 'wb') as fd:
            for chunk in resp.iter_content(chunk_size=65536):
                fd.write(chunk)
    else:
        log.warn(f"Got status code {resp.status_code}")

def get_mesu():
    log = logging.getLogger("mesu")
    log.info("Getting asset files from mesu.apple.com")

    urls = [u.strip() for u in open("urls.txt", "r").readlines()]

    if os.path.isdir("assets"): shutil.rmtree("assets")
    if os.path.isdir("systemassets"): shutil.rmtree("systemassets")

    for url in urls:
        assert url.startswith("https://mesu.apple.com/")
        path = url[len("https://mesu.apple.com/"):]
        # turn "carrierseedios7/assets/foo" into "assets/carrierseedios7/foo" to avoid new toplevel dirs
        path = re.sub('^(carrier[^/]+)/assets/', r'assets/\1/', path)
        path = pathlib.PurePosixPath(path)
        os.makedirs(path.parent, exist_ok=True)

        download(url, path, cachebust=True)

    download(
        "https://itunes.apple.com/WebObjects/MZStore.woa/wa/com.apple.jingle.appserver.client.MZITunesClientCheck/version",
        "assets/iOSIPSW_A.xml",
        cachebust=True)
    # AirPort software update
    download("http://apsu.apple.com/version.xml", "assets/apsu-version.xml")

MS_STORE_APPS = [
    "https://apps.microsoft.com/store/detail/apple-music-preview/9PFHDD62MXS1",
    "https://apps.microsoft.com/store/detail/apple-tv-preview/9NM4T8B9JQZ1",
    "https://apps.microsoft.com/store/detail/apple-devices-preview/9NP83LWLPZ9K",
    "https://apps.microsoft.com/store/detail/itunes/9PB2MZ1ZMB1S",
    "https://apps.microsoft.com/store/detail/icloud/9PKTQ5699M62",
]
def ms_store():
    log = logging.getLogger("msstore")
    #log.setLevel(logging.DEBUG)

    os.makedirs("msstore", exist_ok=True)

    for app_url in MS_STORE_APPS:
        m = re.fullmatch('https://apps.microsoft.com/store/detail/([^/]+)/[0-9A-Z]+', app_url)
        assert m
        app_name = m.group(1)

        success = False
        attempts = 5
        while not success:
            if attempts == 0:
                log.warn(f"Failed after multiple attempts, skipping app {app_name}")
                break
            attempts -= 1

            log.info(f"Trying to get info for app {app_name}")
            resp = sess.post(
                "https://store.rg-adguard.net/api/GetFiles",
                data={"type": "url", "url": app_url, "ring": "RP"}
            )
            if resp.status_code != 200:
                log.warning(f"Status code is {resp.status_code}, retrying...")
                continue
            if 'The server returned an empty list.' in resp.text:
                log.warning(f"Server returned empty list, retrying...")
                continue

            success = True
            lines = resp.text.split("\n")
            result = []
            for line in lines:
                # this is *gross*, I should use an HTML parser
                m = re.search(r'<td><a href="([^"]+)"[^>]*>([^<]+)</a></td><td(?: [^>]*)>[^<]*</td><td(?: [^>]*)>([^<]+)</td>', line)
                if m:
                    result.append({
                        "url": m.group(1),
                        "filename": m.group(2),
                        "sha1": m.group(3)
                    })

            # load existing json file into a dict, or empty dict if it doesn't exist
            try:
                with open(f"msstore/{app_name}.json", "r") as json_file:
                    old_result = json.load(json_file)
            except FileNotFoundError:
                old_result = []
            old_result_d = {a['filename']: a for a in old_result}

            for file_info in result:
                # find the info for this same store file in the old json
                old_file_info = old_result_d.get(file_info['filename'])
                if old_file_info:
                    # Strip out the signature from both old and new URLs.
                    # If they're the same then the signature is the only thing that changed;
                    # keep the old URL.
                    old_url = old_file_info['url']
                    new_url = file_info['url']
                    old_url = re.sub('([?&]P1)=\d+', '\1=nnnn', old_url)
                    new_url = re.sub('([?&]P1)=\d+', '\1=nnnn', new_url)
                    old_url = re.sub('([?&]P4)=[A-Za-z0-9%]+', '\1=nnnn', old_url)
                    new_url = re.sub('([?&]P4)=[A-Za-z0-9%]+', '\1=nnnn', new_url)
                    if file_info['url'] == old_file_info['url']:
                        log.debug(f"for file {file_info['filename']} the URLs are the same")
                    else:
                        if old_url == new_url:
                            log.debug(f"for file {file_info['filename']} the URLs only differ in signature, keeping old one")
                            log.debug(f"old: {old_file_info['url']}")
                            log.debug(f"new: {file_info['url']}")
                            file_info['url'] = old_file_info['url']

            if result == old_result:
                log.info("Success, no changes")
            else:
                log.info("Success, changes found, writing new file")
                # write out the new json file with the filtered results
                with open(f"msstore/{app_name}.json", "w") as json_file:
                    json.dump(result, json_file, indent=4)

    subprocess.check_call(["git", "add", "-A", "msstore"])

get_mesu()

# keep doing the rest in bash
subprocess.check_call(["./update.sh"])

# new stuff
ms_store()
