#!/bin/bash

nocache="cachebust=$(date +%s)"

# These XML files keep appearing as 0-byte-size files, due to some problem on the CDN.
# XXX disable this workaround temporarily, to see if it keeps happening...
if false; then
for dumb_file in \
    assets/com_apple_MobileAsset_UARP_A2458/com_apple_MobileAsset_UARP_A2458.xml \
    assets/com_apple_MobileAsset_MobileBluetoothAssets/com_apple_MobileAsset_MobileBluetoothAssets.xml
do
    if test -f $dumb_file -a ! -s $dumb_file;
    then
        rm $dumb_file
    fi
done
fi

git add -A assets systemassets

# bags
rm -rf bags
mkdir bags
(cd bags;
# push notifications
wget -4 --no-check-certificate -O "init-push-bag.plist" "https://init.push.apple.com/bag"
wget -4 --no-check-certificate -O "init-push-bag-sandbox.plist" "https://init.sandbox.push.apple.com/bag"
wget -4 -O "init-p01st-push-bag.plist" "http://init-p01st.push.apple.com/bag"
wget -4 -O "init-s01st-push-bag.plist" "http://init-s01st.push.apple.com/bag"
# madrid
wget -4 -O "init-p01md-bag.plist" "http://init-p01md.apple.com/bag"
wget -4 -O "init-s01md-bag.plist" "http://init-s01md.apple.com/bag"

# configuration.apple.com
wget -4 https://configuration.apple.com/configurations/pep/config/geo/networkDefaults-ios-{7.1,8.0,8.3,9.0,10.0,10.1,11.0,12.0,12.2}.plist
wget -4 https://configuration.apple.com/configurations/pep/config/geo/networkDefaults-osx-10.{9..15}.plist
wget -4 https://configuration.apple.com/configurations/pep/config/pepconfig.plist
wget -4 https://configuration.apple.com/configurations/internetservices/bt/bcwv.plist
wget -4 https://configuration.apple.com/configurations/internetservices/ckkeyrolld/ckkeyrolld-1.0.plist
wget -4 https://configuration.apple.com/configurations/internetservices/cloudkit/clouddocs/clouddocs-{1.0,1.1}.plist
wget -4 https://configuration.apple.com/configurations/internetservices/cloudkit/cloudphotos/cloudphotos-1.0.plist
wget -4 https://configuration.apple.com/configurations/internetservices/cloudkit/history/history-1.0.plist
wget -4 https://configuration.apple.com/configurations/internetservices/cloudkit/knowledgestore/com.apple.coreduet.knowledge.Sync3Policy.plist
wget -4 https://configuration.apple.com/configurations/internetservices/cloudkit/knowledgestore/com.apple.coreduet.knowledge.syncPolicies2.ios.plist
wget -4 https://configuration.apple.com/configurations/internetservices/cloudkit/knowledgestore/com.apple.coreduet.knowledge.syncPolicies2.macos.plist
wget -4 https://configuration.apple.com/configurations/internetservices/cloudkit/knowledgestore/com.apple.coreduet.knowledge.syncPolicies2.watchos.plist
wget -4 https://configuration.apple.com/configurations/internetservices/cloudkit/large-attachments-1.0.plist
wget -4 https://configuration.apple.com/configurations/internetservices/cloudkit/notes-1.{0..7}.plist
wget -4 https://configuration.apple.com/configurations/internetservices/cloudkit/reminders-1.0.plist
wget -4 https://configuration.apple.com/configurations/internetservices/cloudkit/siri-1.0.plist
wget -4 https://configuration.apple.com/configurations/internetservices/cloudkit/textinput/textinput-1.0.plist
wget -4 https://configuration.apple.com/configurations/internetservices/exchangesync/oauth-migration-configuration.plist
wget -4 https://configuration.apple.com/configurations/internetservices/safari/AutoFillQuirks-1.plist
wget -4 https://configuration.apple.com/configurations/internetservices/safari/KnownSitesUsingPlugIns-1.plist
wget -4 https://configuration.apple.com/configurations/internetservices/safari/SafeBrowsingRemoteConfiguration-0.plist
wget -4 -O mobileme-content-1.0.plist https://gateway.icloud.com/configuration/configurations/internetservices/mobileme/content/content-1.0.plist
wget -4 -O rosetta-configuration.plist https://configuration.apple.com/configurations/internetservices/rosetta/1/configuration.plist
wget -4 -O virtualization-configuration.plist https://configuration.apple.com/configurations/virtualization/1/configuration.plist
wget -4 -O iwork-RemoteDefaults.plist https://configuration.apple.com/configurations/internetservices/iworkapps/RemoteDefaults.plist
wget -4 -O tectosilicate-RemoteDefaults.plist https://configuration.apple.com/configurations/internetservices/tectosilicate/RemoteDefaults.plist
wget -4 -O mail-configuration.plist https://configuration.apple.com/configurations/internetservices/mail/configuration.plist
wget -4 -O fmf-localizedURLs-3.0.plist https://configuration.apple.com/configurations/internetservices/icloud/fmf/localizedURLs-3.0.plist
wget -4 -O HomeKit-ManufacturerDatabase.plist https://configuration.apple.com/configurations/internetservices/HomeKit/v1/ManufacturerDatabase.plist

# CloudKit
wget -4 https://gateway.icloud.com/configuration/configurations/internetservices/cloudkit/cloudkit-1.0.plist

# Cabana?
wget -4 --no-check-certificate -O cabana-bootstrap-31e8c871-9a82-4a76-af31-7857bae5b03e.json https://cabana-config.cdn-apple.com/static/v1/bootstrap-31e8c871-9a82-4a76-af31-7857bae5b03e.json

# mensura
wget -4 -O mensura-config.json https://mensura.cdn-apple.com/api/v1/gm/config
# filter out a geodns'd hostname
sed -i '/test_endpoint/s/"[a-z0-9-]*\.aaplimg\.com/"xxx.aaplimg.com/g' mensura-config.json

# iTunes
wget -4 -O itunes-bag.xml https://init.itunes.apple.com/bag.xml
wget -4 -O itunes-bag-ix6.xml https://init.itunes.apple.com/bag.xml?ix=6
# filter out a dynamically updated timestamp
sed -i '/<key>timestamp<\/key>/s/[0-9]/n/g' itunes-bag.xml itunes-bag-ix6.xml

# keyvalue
wget -4 -O keyvalueservice-config.plist "https://keyvalueservice.icloud.com/config?service-id=iOS"
# filter out a dynamically updated timestamp
sed -i '/<key>timestamp<\/key>/,+1s/[0-9]/n/g' keyvalueservice-config.plist

# GSA
wget -4 --no-check-certificate -O gsa-GsService2-lookup.plist --header="X-MMe-Client-Info: <iPhone6,1> <iPhone OS;12.4.8;16G201> <com.apple.akd/1.0 (com.apple.akd/1.0)>" --user-agent="mesu-archive" https://gsa.apple.com/grandslam/GsService2/lookup
wget -4 --no-check-certificate -O gsa-GsService2-lookup-iOS15.plist --header="X-MMe-Client-Info: <iPhone6,1> <iPhone OS;15.0;16G201> <com.apple.akd/1.0 (com.apple.akd/1.0)>" --user-agent="mesu-archive" https://gsa.apple.com/grandslam/GsService2/lookup
wget -4 --no-check-certificate -O gsa-GsService2-lookup-iOS16.plist --header="X-MMe-Client-Info: <iPhone6,1> <iPhone OS;16.0;16G201> <com.apple.akd/1.0 (com.apple.akd/1.0)>" --user-agent="mesu-archive" https://gsa.apple.com/grandslam/GsService2/lookup
python3 ../sort-plist.py gsa-GsService2-lookup.plist gsa-GsService2-lookup-iOS15.plist gsa-GsService2-lookup-iOS16.plist

# GDMF
wget -4 --no-check-certificate -O gdmf-pmv.json https://gdmf.apple.com/v2/pmv
python3 -c 'import json; json.dump(json.load(open("gdmf-pmv.json", "r")), open("gdmf-pmv.json", "w"), indent=2, sort_keys=True)'
sed -i 's/"ExpirationDate": "[0-9-]*"/"ExpirationDate": "yyyy-mm-dd"/' gdmf-pmv.json

# setup.icloud.com
mkdir icloud-init
# These plists have different content depending on version.
# In most cases it only depends on the major version,
# but there are also differences between <=9.2 and >=9.3, and for 5.0.
for v in {5..16}.0 5.1 9.2 9.3; do
    for context in buddy settings; do
        wget -4 -O icloud-init/init-$context-$v.plist --header="X-MMe-Client-Info: <iNoot4,20> <iPhone OS;$v;whatever> <mesu-archive>" --user-agent="mesu-archive" https://setup.icloud.com/configurations/init?context=$context

        # If removing "p97-" from the file makes it identical to the previous one,
        # then undo the change, because it means it's reverting to an old version,
        # possibly due to server-side caching. If there's any other change then we keep it.
        # Do this until nov 15th, where I hope the caching issues are over.
        if test $(date +%Y%m%d) -lt 20221115; then
            if cmp <(sed 's,p97-acsegateway,acsegateway,' icloud-init/init-$context-$v.plist) <(git show :./icloud-init/init-$context-$v.plist); then
                git checkout icloud-init/init-$context-$v.plist
            fi
        fi
    done
done
)

rm -rf pancake
mkdir -p pancake
(cd pancake
wget -O coremedia.plist https://pancake.apple.com/bags/maple/coremedia
wget -O coreaudio.plist https://pancake.apple.com/bags/maple/coreaudio
wget -O avconference.plist https://pancake.apple.com/bags/maple/ios/avconference
wget -O tu.plist https://pancake.apple.com/bags/maple/ios/tu
wget -O wings.plist https://pancake.apple.com/bags/maple/wings
wget -O cmremoteconfig1.plist https://pancake.apple.com/cmremoteconfig/1/default
wget -O cmremoteconfig2.plist https://pancake.apple.com/cmremoteconfig/2/default
wget -O cmremoteconfig2-airplay.plist https://pancake.apple.com/cmremoteconfig/2/airplay
)

for f in \
    bags/init-[ps]01md-bag.plist \
    bags/init-[ps]01st-push-bag.plist \
    pancake/*.plist;
do
    python3 decode-bag.py $f ${f%.plist}-decoded.plist
done

mkdir -p tmp

# ESS
wget -4 -O "bags/ess-cert-1.0.plist" http://static.ess.apple.com/identity/validation/cert-1.0.plist
wget -4 -O "tmp/VCInit-bag.plist" "http://init.ess.apple.com/WebObjects/VCInit.woa/wa/getBag?ix=4"
python3 decode-bag.py tmp/VCInit-bag.plist bags/VCInit-bag-decoded.plist
# Whatever this is...
wget -4 -O "tmp/PdsInit-bag.plist" "http://pds-init.ess.apple.com/WebObjects/PdsInit.woa/wa/getBag?ix=3"
python3 decode-bag.py tmp/PdsInit-bag.plist bags/PdsInit-bag-decoded.plist

# transparencyd
wget -4 --no-check-certificate -O tmp/transparencyd-bag.plist "https://init-kt.apple.com/init/getBag?ix=3"
python3 decode-bag.py tmp/transparencyd-bag.plist bags/transparencyd-bag-decoded.plist

# GameKit
wget -4 -O tmp/GKInit-bag1.plist "http://init.gc.apple.com/WebObjects/GKInit.woa/wa/getBag?ix=1"
wget -4 -O tmp/GKInit-bag2.plist "http://init.gc.apple.com/WebObjects/GKInit.woa/wa/getBag?ix=2"
python3 decode-bag.py tmp/GKInit-bag1.plist bags/GKInit-bag1-decoded.plist
python3 decode-bag.py tmp/GKInit-bag2.plist bags/GKInit-bag2-decoded.plist

# these values change dynamically so we filter them out
sed -E -i '/<key>(vc-registration-hbi|bag-expiry-timestamp)<\/key><integer>/s/[0-9]/n/g;
/<key>gk-commnat-(main0|main1|cohort)<\/key><string>/s/17\.[0-9]+\.[0-9]+\.[0-9]+/17.x.x.x/;
/<key>vc-build-version<\/key>/s,<string>[^<]*</string>,<string>nnnn</string>,' bags/VCInit-bag-decoded.plist
sed -E -i '/<key>pds-build-version<\/key>/s,<string>[^<]*</string>,<string>nnnn</string>,' bags/PdsInit-bag-decoded.plist
sed -E -i '/<key>bag-expiry-timestamp<\/key><integer>/s/[0-9]/n/g; /<key>uuid<\/key>/s/<string>[0-9a-f-]+/<string>.../' bags/transparencyd-bag-decoded.plist
sed -E -i '/<key>gk-build-version<\/key>/s,<string>[^<]*</string>,<string>nnnn</string>,' bags/GKInit-bag?-decoded.plist

git add -A bags pancake

rm -r support
mkdir support
(cd support;
wget -O HT201222.html https://support.apple.com/en-us/HT201222
for id in $(sed -n '/<td><a href/s,.*a href="https\?://support.apple.com/kb/\(HT[0-9]\+\)">.*,\1,p' HT201222.html) \
    HT201222 \
    HT201624 \
    HT213317 \
    HT213325 \
    HT213110 \
    HT213245 \
    HT211081 \
    HT208714 \
    HT201296 HT201471 HT209580 HT204217 HT204507 \
    HT201608 HT201300 HT201634 HT202888 HT213073 \
    HT200008 HT211109 \
    HT213481 HT213545 HT208709 HT207057 \
    HT201260 \
; do
    wget -O $id.html https://support.apple.com/en-us/$id
    sed -i '/link rel="alternate"/,+1d' $id.html

    if ! diff -u <(git show :./$id.html) $id.html; then
        if test $(date +%Y%m%d) -lt 20230107 && diff -u -w <(git show :./$id.html) <(cat $id.html | sed 's/2022 Apple Inc/2023 Apple Inc/') > /dev/null; then
            # Apple updated the copyright footer on support pages,
            # but due to caching some keep reverting.
            # If old file and new file differ, but the only difference is
            # copyright rolling back to 2022, then revert back to old file.
            # Keep this up for a week, by which time I hope the cache stops flapping.
            echo "reverting $id.html because only change is copyright date rolling back";
            git checkout $id.html
        elif ! [ -s $id.html ]; then
            # if new file is empty, revert back to old file; could have been network error downloading
            echo "reverting $id.html because it's empty";
            git checkout $id.html
        fi
    fi
done
wget -O developer-releases.html https://developer.apple.com/news/releases/
wget -O salesdownload-compliance.html http://salesdownload.apple.com/public/sites/asw/common/compliance/index.htm
wget -O euro-compliance.html https://www.apple.com/euro/compliance/
wget -O euro-compliance-archive.html https://www.apple.com/euro/compliance/archive.html
wget -O euro-compliance-beats.html https://www.apple.com/euro/compliance/beats/
wget -O euro-compliance-beats-archive.html https://www.apple.com/euro/compliance/beats/archive.html
wget -O euro-compliance-617.html https://www.apple.com/euro/compliance/617_2013/

# make diffs more readable
sed 's,><td ,>\n<td ,g' salesdownload-compliance.html > salesdownload-compliance-format.html

wget -O rfexposureV2.json           https://www.apple.com/legal/rfexposure/index/hierarchyv2/dataV2.json
wget -O rfexposureV2-archive.json   https://www.apple.com/legal/rfexposure/archive/index/hierarchyv2/dataV2.json
python3 -c 'import json; json.dump(json.load(open("rfexposureV2.json", "r")), open("rfexposureV2.json", "w"), indent=2)'
python3 -c 'import json; json.dump(json.load(open("rfexposureV2-archive.json", "r")), open("rfexposureV2-archive.json", "w"), indent=2)'
)
git add -A support

# Apple Pay
rm -r smp-content
mkdir smp-content
(cd smp-content
for f in region/v2/{config,config-alt,config-crt}.json discovery/v1/manifest.json; do
    dirpath=${f%/*}
    mkdir -p $dirpath
    wget -O $f https://smp-device-content.apple.com/static/$f
done
)
git add -A smp-content

if false; then
rm -r geoservices
mkdir geoservices
(cd geoservices;
# These manifests have different content depending on version.
# In most cases it only depends on the major version,
# but there are also differences on 8.3, 9.3, 10.1, 10.3, 13.4, 14.2, 14.5;
# so it's better to grab all minor versions too.
for v in \
    7.{0..1} \
    8.{0..4} \
    9.{0..3} \
    10.{0..3} \
    11.{0..4} \
    12.{0..5} \
    13.{0..7} \
    14.{0..8} \
    15.{0..5} \
    16.0;
do
    wget -4 -O manifest-$v.pb "https://gspe35-ssl.ls.apple.com/geo_manifest/dynamic/config?os=ios&os_version=$v"
done
)
git add -A geoservices
fi
