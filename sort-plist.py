#!/usr/bin/python3

import plistlib
import sys

for filename in sys.argv[1:]:
    with open(filename, "rb") as in_f:
        data = plistlib.load(in_f)

    new_plist = plistlib.dumps(data, sort_keys=True)
    with open(filename, "wb") as out_f:
        out_f.write(new_plist.replace(b'\t', b'    '))
