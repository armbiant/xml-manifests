#!/usr/bin/python3

import sys
import plistlib
import base64

with open(sys.argv[1], "rb") as in_file:
    parsed_dict = plistlib.load(in_file)

bag_data = parsed_dict['bag']
if isinstance(bag_data, str):
    output_data = base64.b64decode(bag_data.encode('ascii'))
else:
    output_data = bag_data

with open(sys.argv[2], "wb") as out_file:
    out_file.write(output_data)
